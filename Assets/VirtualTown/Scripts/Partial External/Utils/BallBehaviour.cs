using System;
using UnityEngine;

public partial class BallBehaviour : MonoBehaviour
{
    [SerializeField]
    private float delayToRestore = 1.5f;

    [SerializeField]
    private ForceMode forceMode = ForceMode.Impulse;

    public Action OnScore;

    public Action<Vector3> OnThrow;

    [SerializeField]
    private AnimationCurve curveZ, curveY;

    [SerializeField]
    private bool forceValues = false;

    [SerializeField]
    private float forcez, forcey;
}
