using UnityEngine;

public partial class OpenOnClick : MonoBehaviour
{
    [SerializeField] private string location;
    [SerializeField] private string message;

    [SerializeField]
    private float timeActive = 3;
}
