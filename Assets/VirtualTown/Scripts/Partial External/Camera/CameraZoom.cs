﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public partial class CameraZoom : MonoBehaviour
{
    [SerializeField]
    private float minFOV = 20f;

    [SerializeField]
    private float maxFOV = 100f;

    [SerializeField]
    private float deltaFOVByStep = 20f;
}
