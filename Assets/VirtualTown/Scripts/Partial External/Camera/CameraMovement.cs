using UnityEngine;

namespace VirtualTown
{
    public partial class CameraMovement : MonoBehaviour
    {
        [SerializeField]
        public Vector3 offset = new Vector3(0.7f, 2.2f, -2.5f);

        [SerializeField]
        public Vector3 angularMouseVelocity = new Vector3(0f, 20f, 0f);

        [SerializeField]
        private LayerMask layerBlockCamera;

        [SerializeField]
        public float translateVelocity = 5f;

        [SerializeField]
        private float offsetHeightLookAt = 2f;

        [SerializeField]
        public float maxDistanceZoom;

        [SerializeField]
        public float minDistanceZoom;

        [SerializeField]
        public int amountStepsZooms = 4;

        [SerializeField]
        public float maxAngleX = 35f;

        public bool isLock = true;
    }
}