﻿using UnityEngine;

namespace VirtualTown
{
    public partial class RoomController : MonoBehaviour//SingletonBehaviour<RoomController>
    {
        public Vector3 initalPosition;
        public float randomRangeAround = 4f;

        [SerializeField]
        private bool ignoreCamDefinitions = false;

        [SerializeField] private CameraMovement cameraController;
        [SerializeField] private Camera freeCamera;
        [SerializeField] private Camera fixedCamera;

    }
}