using UnityEngine;

namespace VirtualTown.Controllers
{
    public partial class LeaderboardController : MonoBehaviour
    {
        [SerializeField]
        private float deltaTimeLeaderboard = 60f;

        [SerializeField]
        private Transform[] leaderboardsPoints;
    }
}
