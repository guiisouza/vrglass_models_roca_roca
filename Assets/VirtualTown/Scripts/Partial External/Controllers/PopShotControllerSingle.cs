using UnityEngine;

public partial class PopShotControllerSingle : MonoBehaviour
{
    [SerializeField]
    private Transform ballTransform;

    [SerializeField]
    private Vector3 cameraPosition, cameraRotation;

    [SerializeField]
    private Transform lookAtBall;

    [SerializeField]
    private ParticleSystem[] particlesScore = new ParticleSystem[0];
}
