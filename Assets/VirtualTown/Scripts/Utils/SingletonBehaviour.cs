﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Auxiliar Abstract Class with Singleton pattern implemented.
/// </summary>
/// <typeparam name="T"></typeparam>
public abstract class SingletonBehaviour<T> : MonoBehaviour where T : SingletonBehaviour<T>
{
    private static bool isInstanced = false;
    private static T instance;

    public static T Instance
    {
        get
        {
            if (!isInstanced)
            {
                instance = FindObjectOfType<T>();

                if (instance == null)
                {
                    instance = new GameObject(
                        typeof(T).Name, typeof(T)
                    ).GetComponent<T>();
                }

                isInstanced = instance != null;

                if (!isInstanced)
                    throw new NullReferenceException("Component of Type " + typeof(T).Name + "  not Found in Scene " + SceneManager.GetActiveScene().name);

				instance.Initialize();
            }

            return instance;
        }
    }
	public virtual bool IsDontDestroyOnLoad { get => true; }
   
	protected virtual void Awake()
    {
		if(IsDontDestroyOnLoad)
			DontDestroyOnLoad(this);
    }

	public virtual void Initialize () { }
}
