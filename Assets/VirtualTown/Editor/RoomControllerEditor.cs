﻿using UnityEngine;
using UnityEditor;

namespace VirtualTown.VirtualEditor
{
    [CustomEditor(typeof(VirtualTown.RoomController))]
    public class RoomControllerEditor : Editor
    {
        private VirtualTown.RoomController instance;

        private void OnSceneGUI()
        {
            Color cacheColor = Handles.color;
            Handles.color = Color.yellow;
            instance = this.target as RoomController;
            Handles.DrawWireDisc(instance.initalPosition, Vector3.up, instance.randomRangeAround);
            Handles.color = cacheColor;
        }
    }
}