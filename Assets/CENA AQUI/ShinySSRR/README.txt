﻿********************************
*          SHINY - SSRR        *
*     Created by Kronnect      * 
*          README FILE         *
********************************


This package is designed for the built-in pipeline (for URP, please remove this one and import the other package included in the bundle).
Requirements: Unity 2019.4 or later

How to use the asset
--------------------
Add ShinySSRR to the camera where you want reflections to be visible


Help & Support Forum
--------------------

Check the Documentation (PDF) for detailed instructions:

Have any question or issue?
* Email: contact@kronnect.com
* Support Forum: http://kronnect.com
* Twitter: @KronnectGames

If you like Shiny, please rate it on the Asset Store. It encourages us to keep improving it! Thanks!


Future updates
--------------

All our assets follow an incremental development process by which a few beta releases are published on our support forum (kronnect.com).
We encourage you to signup and engage our forum. The forum is the primary support and feature discussions medium.

Of course, all updates of the asset will be eventually available on the Asset Store.



More Cool Assets!
-----------------
Check out our other assets here:
https://assetstore.unity.com/publishers/15018



Version history
---------------

Version 3.0
- Redesigned reflection blur with simpler settings and contact hardening option
- New pyramid-blur for contact hardening/distance blur
- Rearranged inspector options
- Material smoothness now reduces fuzzyness

Version 2.6
- Ability to customize the material property names for smoothness map & normal map in Reflections script

Version 2.5
- Added support for smoothness map when using metallic workflow in forward rendering path

Version 1.1
- Max Reflections Multiplier increased to 2
- Added Reflections Min and Max clamping parameters

Version 1.0
- First release for the built-in pipeline based on the URP version feature set
