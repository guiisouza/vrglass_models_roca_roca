﻿using UnityEngine;
using UnityEditor;

namespace ShinySSRR {

    [CustomEditor(typeof(ShinySSRR))]
    public class RenderFeatureEditor : Editor {

        SerializedProperty showInSceneView;
        SerializedProperty reflectionsMultiplier, reflectionsMinIntensity, reflectionsMaxIntensity, downsampling, depthBias, outputMode, separationPos, lowPrecision;
        SerializedProperty sampleCount, stepSize, binarySearchIterations, thickness, decay, jitter, animatedJitter;
        SerializedProperty fresnel, fuzzyness, contactHardening, minimumBlur;
        SerializedProperty blurDownsampling, specularControl, specularSoftenPower, vignetteSize;
        Reflections[] reflections;
        public Texture bulbOnIcon, bulbOffIcon, deleteIcon, arrowRight;

        private void OnEnable() {
            showInSceneView = serializedObject.FindProperty("showInSceneView");
            reflectionsMultiplier = serializedObject.FindProperty("reflectionsMultiplier");
            reflectionsMinIntensity = serializedObject.FindProperty("reflectionsMinIntensity");
            reflectionsMaxIntensity = serializedObject.FindProperty("reflectionsMaxIntensity");
            downsampling = serializedObject.FindProperty("downsampling");
            depthBias = serializedObject.FindProperty("depthBias");
            outputMode = serializedObject.FindProperty("outputMode");
            separationPos = serializedObject.FindProperty("separationPos");
            lowPrecision = serializedObject.FindProperty("lowPrecision");
            sampleCount = serializedObject.FindProperty("sampleCount");
            stepSize = serializedObject.FindProperty("stepSize");
            binarySearchIterations = serializedObject.FindProperty("binarySearchIterations");
            thickness = serializedObject.FindProperty("thickness");
            decay = serializedObject.FindProperty("decay");
            fresnel = serializedObject.FindProperty("fresnel");
            fuzzyness = serializedObject.FindProperty("fuzzyness");
            contactHardening = serializedObject.FindProperty("contactHardening");
            minimumBlur = serializedObject.FindProperty("minimumBlur");
            jitter = serializedObject.FindProperty("jitter");
            animatedJitter = serializedObject.FindProperty("animatedJitter");
            blurDownsampling = serializedObject.FindProperty("blurDownsampling");
            specularControl = serializedObject.FindProperty("specularControl");
            specularSoftenPower = serializedObject.FindProperty("specularSoftenPower");
            vignetteSize = serializedObject.FindProperty("vignetteSize");

#if UNITY_2020_1_OR_NEWER
            reflections = FindObjectsOfType<Reflections>(true);
#else
            reflections = FindObjectsOfType<Reflections>();
#endif
        }

        public override void OnInspectorGUI() {

            ShinySSRR shiny = (ShinySSRR)target;
            int reflectionsCount = reflections != null ? reflections.Length : 0;

            serializedObject.Update();

            EditorGUILayout.PropertyField(showInSceneView);
            EditorGUILayout.PropertyField(downsampling);
            if (downsampling.intValue > 1 && !shiny.isDeferredActive) {
                EditorGUI.indentLevel++;
                EditorGUILayout.PropertyField(depthBias);
                EditorGUI.indentLevel--;
            }
            EditorGUILayout.PropertyField(outputMode);
            if (outputMode.intValue == (int)OutputMode.SideBySideComparison) {
                EditorGUI.indentLevel++;
                EditorGUILayout.PropertyField(separationPos);
                EditorGUI.indentLevel--;
            }
            EditorGUILayout.PropertyField(lowPrecision);

            EditorGUILayout.PropertyField(sampleCount);
            EditorGUILayout.PropertyField(stepSize);
            EditorGUILayout.PropertyField(binarySearchIterations);
            EditorGUILayout.PropertyField(thickness);
            EditorGUILayout.PropertyField(jitter);
            EditorGUILayout.PropertyField(animatedJitter);

            EditorGUILayout.PropertyField(reflectionsMultiplier, new GUIContent("Global Multiplier"));
            EditorGUILayout.PropertyField(reflectionsMinIntensity, new GUIContent("Min Intensity"));
            EditorGUILayout.PropertyField(reflectionsMaxIntensity, new GUIContent("Max Intensity"));
            EditorGUILayout.PropertyField(fresnel);
            EditorGUILayout.PropertyField(decay);
            EditorGUILayout.PropertyField(specularControl);
            if (specularControl.boolValue) {
                EditorGUILayout.PropertyField(specularSoftenPower);
            }
            EditorGUILayout.PropertyField(vignetteSize);

            EditorGUILayout.PropertyField(fuzzyness, new GUIContent("Fuzziness"));
            EditorGUILayout.PropertyField(contactHardening);
            EditorGUILayout.PropertyField(minimumBlur);
            EditorGUILayout.PropertyField(blurDownsampling);

            if (reflectionsCount > 0) {
                if (!shiny.isDeferredActive) {
                    EditorGUILayout.HelpBox("Some settings may be overriden by Reflections scripts on specific objects.", MessageType.Info);
                }
                EditorGUILayout.Separator();
                EditorGUILayout.LabelField("Reflections scripts in Scene", EditorStyles.helpBox);
                if (shiny.isDeferredActive) {
                    EditorGUILayout.HelpBox("In deferred rendering path, only global SSR settings are used.", MessageType.Warning);
                }
                for (int k = 0; k < reflectionsCount; k++) {
                    Reflections refl = reflections[k];
                    if (refl == null) continue;
                    EditorGUILayout.BeginHorizontal();
                    GUI.enabled = refl.gameObject.activeInHierarchy;
                    if (GUILayout.Button(new GUIContent(refl.enabled ? bulbOnIcon : bulbOffIcon, "Toggle on/off this reflection"), EditorStyles.miniButton, GUILayout.Width(35))) {
                        refl.enabled = !refl.enabled;
                    }
                    GUI.enabled = true;
                    if (GUILayout.Button(new GUIContent(deleteIcon, "Remove this reflection script"), EditorStyles.miniButton, GUILayout.Width(35))) {
                        if (EditorUtility.DisplayDialog("Confirmation", "Remove the reflection script on " + refl.gameObject.name + "?", "Ok", "Cancel")) {
                            DestroyImmediate(refl);
                            reflections[k] = null;
                            continue;
                        }
                    }
                    if (GUILayout.Button(new GUIContent(arrowRight, "Select this reflection script"), EditorStyles.miniButton, GUILayout.Width(35), GUILayout.Width(40))) {
                        Selection.activeObject = refl.gameObject;
                        EditorGUIUtility.PingObject(refl.gameObject);
                        GUIUtility.ExitGUI();
                    }
                    GUI.enabled = refl.isActiveAndEnabled;
                    if (!refl.gameObject.activeInHierarchy) {
                        GUILayout.Label(refl.name + " (hidden gameobject)");
                    } else {
                        GUILayout.Label(refl.name);
                    }
                    GUI.enabled = true;
                    EditorGUILayout.EndHorizontal();
                }
            } else if (reflectionsCount == 0) {
                if (!shiny.isDeferredActive) {
                    EditorGUILayout.Separator();
                    EditorGUILayout.LabelField("Reflections in Scene", EditorStyles.helpBox);
                    EditorGUILayout.HelpBox("In forward rendering path, add a Reflections script to any object or group of objects that you want to get reflections.", MessageType.Info);
                }

            }

            serializedObject.ApplyModifiedProperties();

        }

    }
}