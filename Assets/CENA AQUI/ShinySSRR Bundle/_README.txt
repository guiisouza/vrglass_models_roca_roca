
This asset contains 2 main packages. Please use the appropriate package according to the rendering pipeline of your project:

** Built-in pipeline **
- Import the package from the folder /Builtin.

** Universal Rendering Pipeline **
- Import the package from the folder /URP


Once imported, read the PDF documentation for additional details.
Thanks for using Shiny SSRR.


Questions / Suggestions / Support
---------------------------------
* Website-Forum: https://kronnect.com
* Support: contact@kronnect.com
* Twitter: @KronnectGames

